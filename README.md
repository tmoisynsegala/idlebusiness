# AdVenture Capitalist clone

This project was created with Create React App and
 implemented using pixi.js and greensock, in Typescript
 
Atlas texture created with TexturePacker

## How to run

You can check out the online version of the project at:
http://test-idlebusiness.s3-website.us-east-2.amazonaws.com/index.html

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />

### `yarn build`

Builds the app for production to the `build` folder.<br />

The build is minified and the filenames include the hashes.<br />

## Gameplay

The game plays the same way as AdVenture Capitalist, you start with only money
enough to buy the first store, only one plus button will be enabled,
after clicking the game enable the whole button to be clicked, starting the work
(The green button is on top of the work button,
 this may not be instantly recognizable,
  my wife couldn't figured it out the first time :p).
Notifications will appear bellow the player balance, by clicking them
the upgrades are bought.
If you close the game, when reopening you will have won more money
(only from currently working stores or if you have a manager).

## Code Structure

The code was build using pixijs as a graphical framework and
used greensock just to make some button and notification animations.
The code starts on index.ts just as way to represent the entry of the app and
to kickstart the Game. The game loads up, read the configuration file "game-descriptor.ts" which
contains the assets the game should load, and the configuration of the visual elements that
gets interpreted by the Assembler, generally I tend to use this kind of structure because it
makes all the visual configuration of the game easier, cleans up a lot the clutter of the code,
it's easy to transform this in a data file, making it easier to incorporate an editor,
and can generate a framework to build other games using same components. All the game logic
it's contained in the Game, which also connects all components and handle them, for a small
game it should be OK, but as the game grow it would need other pieces to offload the logic
and behaviors. The game extends an Application which only is there to hold the PIXI configurations
and encapsulate the game loop. All the code contained in the src/base should be more generic and
usable by other project, the code in the src it's more game specific, with this organization
it is possible to make the base a subrepo and share it as pure code between games.
All the other components should be only visual components needed by this game.
The game handle the continuous work by saving when certain action are taken, and marking
the time when the work starts and solving how much time has passed from the last work.

## Problems

There are some of the things that I didn't had time to implement:
    
    - Game resizing
    - Sound
    - Particles
    - Multiply number of buys (feature)

Also I had the initial intention to make a fullstack app, but because of time constraints
I didn't implement the server to handle the game logic and saving the state/session 

