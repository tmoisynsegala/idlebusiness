import * as PIXI from "pixi.js";
import PlayerStats from "../PlayerStats";
import BusinessButton from "../BusinessButton";
import ResourceLoader from "./ResourceLoader";
import NotificationButton from "../NotificationButton";
import {GameComponent} from "./Game";

export default class Assembler {

    /**
     * Decides whether to get the image from atlas or single image
     * @param obj
     */
    private static sprite(obj: { image?: string, atlasImage?: string }): PIXI.Sprite {
        if (obj.image)
            return PIXI.Sprite.from(obj.image);

        if (obj.atlasImage)
            return new PIXI.Sprite(ResourceLoader.getAtlasTexture(obj.atlasImage));

        throw new Error(`Unable to load Sprite ${obj}`);
    }

    /**
     * Instantiate the component
     * @param proto
     */
    private static businessButton(proto:GameComponent){
        if(proto.iconName)
            return new BusinessButton(proto?.iconName);
        else
            throw new Error(`Missing iconName property on ${proto}`);
    }

    /**
     * Holds all the available components that we can instantiate
     */
    static component: Record<string, Function> = {
        'PIXI.Sprite': Assembler.sprite,
        'PIXI.Container': () => new PIXI.Container(),
        'PlayerStats': () => new PlayerStats(),
        'BusinessButton': Assembler.businessButton,
        'NotificationButton': () => new NotificationButton(),
    }

    /**
     * Constructs the interface on top of the passed container
     * @param container
     * @param components
     */
    static build(container: PIXI.Container, components: GameComponent[]) {
        components.forEach(prototype => {
            const spawn = Assembler.component[prototype.component](prototype);

            if(prototype?.name)
                spawn.name = prototype.name;

            if (prototype?.x)
                if (typeof prototype.x === 'string')
                    spawn.x = parseFloat(prototype.x);
                else
                    spawn.x = prototype.x;

            if (prototype?.y)
                if (typeof prototype.y === 'string')
                    spawn.y = parseFloat(prototype.y);
                else
                    spawn.y = prototype.y;

            if(prototype?.scale)
                spawn.scale.set(prototype.scale);

            if (prototype?.children)
                Assembler.build(spawn, prototype.children);

            container.addChild(spawn);
        })
    }
}