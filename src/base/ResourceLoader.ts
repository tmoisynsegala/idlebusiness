import PIXI from "pixi.js";

/**
 * Currently loading only images, missing support n atlas, sound, spine, etc
 */
export default class ResourceLoader {

    private _loader: PIXI.Loader;
    private static _instance: ResourceLoader;

    constructor(loader: PIXI.Loader) {
        ResourceLoader._instance = this;
        this._loader = loader;
    }

    async init(assets: Array<{ name: string, path: string }>): Promise<PIXI.Loader> {
        return new Promise<PIXI.Loader>((done) => {
            assets.forEach(asset => this._loader.add(asset.name, asset.path));

            this._loader.load(() => {
                done();
            });
        });
    }

    /**
     * Utils functions to make the texture on atlas easily accessible
     * @param name
     */
    static getAtlasTexture(name: string): PIXI.Texture {
        const {textures} = ResourceLoader._instance._loader?.resources?.base;
        if(textures && textures[name])
            return textures[name];
        else
            throw new Error(`ResourceLoader - Missing Texture ${name}`);
    }
}