import * as PIXI from "pixi.js";

/**
 * Holder of lower level mandatory configurations
 * and the ticker
 */
export default class Application {

    public container:PIXI.Container = new PIXI.Container();

    public app:PIXI.Application;

    constructor(backgroundColor:number = 0x1099bb) {
        this.app = new PIXI.Application({
            width: window.innerWidth,
            height: window.innerHeight,
            backgroundColor: backgroundColor,
            resolution: window.devicePixelRatio || 1,
        });
        document.body.style.margin = "0";
        document.body.style.overflow = "hidden";
        document.body.appendChild(this.app.view);
        this.app.stage.addChild(this.container);

        // Listen for animate update
        this.app.ticker.add((delta) => {
            this.update(delta);
        });
    }

    update( delta:number ){

    }
}