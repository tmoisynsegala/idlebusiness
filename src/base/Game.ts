import Application from "./Application";
import {gameDescriptor} from "../game-descriptor";
import ResourceLoader from "./ResourceLoader";
import Assembler from "./Assembler";
import PlayerStats from "../PlayerStats";
import ButtonBusiness, {BusinessConfig} from "../BusinessButton";
import ButtonNotification from "../NotificationButton";
import NotificationButton from "../NotificationButton";

/**
 * Game logic, data necessary and connections between components
 */
export default class Game extends Application {

    private playerData = {
        balance: 10
    }

    private businessData: Record<string, BusinessConfig> = {
        farm: {startTime: 0, isRunning: false, price: 10, count: 0, time: 5, workTime: 5, won: 10},
        store: {startTime: 0, isRunning: false, price: 100, count: 0, time: 10, workTime: 10, won: 100},
        sports: {startTime: 0, isRunning: false, price: 1000, count: 0, time: 30, workTime: 30, won: 1000},
        car: {startTime: 0, isRunning: false, price: 2000, count: 0, time: 60, workTime: 60, won: 2000},
        diamond: {startTime: 0, isRunning: false, price: 5000, count: 0, time: 120, workTime: 120, won: 5000},
        game: {startTime: 0, isRunning: false, price: 10000, count: 0, time: 240, workTime: 240, won: 10000},
        phone: {startTime: 0, isRunning: false, price: 20000, count: 0, time: 480, workTime: 480, won: 20000},
        drug: {startTime: 0, isRunning: false, price: 30000, count: 0, time: 960, workTime: 960, won: 30000},
        plane: {startTime: 0, isRunning: false, price: 50000, count: 0, time: 1920, workTime: 1920, won: 50000},
        rocket: {startTime: 0, isRunning: false, price: 100000, count: 0, time: 3840, workTime: 3840, won: 100000},
    }

    private managers: Record<string, ManagerConfig> = {
        farm: {isOn: false, isSold: false, price: 100, count: 10, info: "Farm Manager"},
        store: {isOn: false, isSold: false, price: 10000, count: 10, info: "Store Manager"},
        sports: {isOn: false, isSold: false, price: 100000, count: 10, info: "Sports Manager"},
        car: {isOn: false, isSold: false, price: 2000000, count: 10, info: "Car Manager"},
        diamond: {isOn: false, isSold: false, price: 5000000, count: 10, info: "Diamond Manager"},
        game: {isOn: false, isSold: false, price: 10000000, count: 10, info: "Game Manager"},
        phone: {isOn: false, isSold: false, price: 20000000, count: 10, info: "Phone Manager"},
        drug: {isOn: false, isSold: false, price: 30000000, count: 10, info: "Drug Manager"},
        plane: {isOn: false, isSold: false, price: 50000000, count: 10, info: "Plane Manager"},
        rocket: {isOn: false, isSold: false, price: 1000000000, count: 10, info: "Rocket Manager"},
    }

    private businessExpansion: Record<string, ExpansionConfig> = {
        farm: {isOn: false, isSold: false, multiplier: 3, price: 100, count: 10, info: "Farm Expansion"},
        store: {isOn: false, isSold: false, multiplier: 3, price: 10000, count: 10, info: "Store Expansion"},
        sports: {isOn: false, isSold: false, multiplier: 3, price: 100000, count: 10, info: "Sports Expansion"},
        car: {isOn: false, isSold: false, multiplier: 3, price: 2000000, count: 10, info: "Car Expansion"},
        diamond: {isOn: false, isSold: false, multiplier: 3, price: 5000000, count: 10, info: "Diamond Expansion"},
        game: {isOn: false, isSold: false, multiplier: 3, price: 10000000, count: 10, info: "Game Expansion"},
        phone: {isOn: false, isSold: false, multiplier: 3, price: 20000000, count: 10, info: "Phone Expansion"},
        drug: {isOn: false, isSold: false, multiplier: 3, price: 30000000, count: 10, info: "Drug Expansion"},
        plane: {isOn: false, isSold: false, multiplier: 3, price: 50000000, count: 10, info: "Plane Expansion"},
        rocket: {isOn: false, isSold: false, multiplier: 3, price: 1000000000, count: 10, info: "Rocket Expansion"},
    }

    private currentNotifications: { manager: ManagerConfig, button: NotificationButton }[] = [];

    private resourceLoader: ResourceLoader;
    private playerStats: PlayerStats | null = null;
    private business: ButtonBusiness[] = [];
    private notifications: ButtonNotification[] = [];

    constructor() {
        super(0x1099bb);
        this.resourceLoader = new ResourceLoader(this.app.loader);
        this.load();
        this.init();
    }

    async init() {
        await this.loadAssets();
        await this.assemble();
        this.getRefs();
        this.start();
    }

    async loadAssets() {
        const assetList = gameDescriptor.assets;
        return await this.resourceLoader.init(assetList);
    }

    async assemble() {
        Assembler.build(this.container, gameDescriptor.components);
    }

    /**
     * Retrieves references to be accessed by the Game
     */
    getRefs() {
        this.playerStats = this.container.getChildByName('playerStats', true) as PlayerStats;
        const businessIds = ['farm', 'store', 'sports', 'car', 'diamond', 'game', 'phone', 'drug', 'plane', 'rocket'];
        businessIds.forEach(id => {
            this.business.push(this.container.getChildByName(id, true) as ButtonBusiness);
        });

        const notificationIds = ["notification1", "notification2", "notification3"];
        notificationIds.forEach(id => {
            this.notifications.push(this.container.getChildByName(id, true) as ButtonNotification);
        });
    }

    start() {
        this.playerStats?.setBalance(this.playerData.balance);

        this.business.forEach(b => {
            let bData = this.businessData[b.name];
            b.config(bData, 0);
            b.onBuy = () => {
                bData.count++;
                this.playerData.balance -= bData.price;

                if (bData.isRunning)
                    return;

                b.setEnable(true);
                this.save();
            }
            b.onWork = () => {
                if (bData.isRunning)
                    return;

                bData.isRunning = true;
                bData.startTime = Date.now();
                b.setEnable(false);
                this.save();
            }

            if (bData.count >= 1 && !this.managers[b.name].isSold)
                b.setEnable(true);
        })
    }

    update(delta: number) {
        this.playerStats?.setBalance(this.playerData.balance);
        this.business.forEach(b => {
            let bData = this.businessData[b.name];
            let manager = this.managers[b.name];
            let expansion = this.businessExpansion[b.name];

            if (manager.isSold && !bData.isRunning)
                b.onWork();

            if (bData.isRunning) {
                b.setEnable(false);

                let multiplyOfflineTime = 1;
                if (manager.isSold)
                    multiplyOfflineTime = Math.floor((Date.now() - bData.startTime) / 1000 / bData.workTime);

                bData.time = bData.workTime - (Date.now() - bData.startTime) / 1000;
                if (bData.time <= 0) {
                    bData.isRunning = false;
                    bData.startTime = 0;
                    bData.time = bData.workTime;
                    if (expansion.isSold)
                        this.playerData.balance += bData.price * bData.count * expansion.multiplier * multiplyOfflineTime;
                    else
                        this.playerData.balance += bData.price * bData.count;

                    b.setEnable(true);
                    this.save();
                }
            }

            this.checkNotification(manager, bData);
            this.checkNotification(expansion, bData);

            let data = {...bData};
            if (expansion.isSold)
                data.won *= expansion.multiplier;
            b.config(data, (data.workTime - data.time) / data.workTime);
            b.setEnablePurchase(this.businessData[b.name].price <= this.playerData.balance);
        });
    }

    private checkNotification(data: ManagerConfig, bData: BusinessConfig) {
        const {count, price} = data;
        if (this.playerData.balance >= price && bData.count >= count) {
            this.notifications.forEach(notification => {
                if (notification.isEmpty() && !data.isOn && !data.isSold) {
                    notification.config(data);
                    this.currentNotifications.push({manager: data, button: notification})
                    data.isOn = true;
                    notification.show();
                    notification.onSold = () => {
                        data.isSold = true;
                        this.playerData.balance -= price;

                        this.removeNotification(notification);
                        this.currentNotifications
                            .concat()
                            .forEach(obj => {
                                    if (obj.manager.price > this.playerData.balance) {
                                        obj.manager.isOn = false;
                                        obj.button.disable();
                                        this.removeNotification(obj.button);
                                    }
                                }
                            );
                        this.save();
                    }
                }
            })
        }
    }

    private removeNotification(notification: NotificationButton) {
        let filterElement = this.currentNotifications.filter(obj => obj.button === notification)[0];
        this.currentNotifications.splice(this.currentNotifications.indexOf(filterElement),1);
    }

    /**
     * Saves player data
     */
    private save() {
        localStorage.setItem('playerData', JSON.stringify(this.playerData));
        localStorage.setItem('managers', JSON.stringify(this.managers));
        localStorage.setItem('businessData', JSON.stringify(this.businessData));
        localStorage.setItem('businessExpansion', JSON.stringify(this.businessExpansion));
    }

    /**
     * Reads the save data
     */
    private load() {
        const playerData = localStorage.getItem('playerData');
        const managers = localStorage.getItem('managers');
        const businessData = localStorage.getItem('businessData');
        const businessExpansion = localStorage.getItem('businessExpansion');

        this.playerData = playerData ? JSON.parse(playerData) : this.playerData;
        this.managers = managers ? JSON.parse(managers) : this.managers;
        this.businessData = businessData ? JSON.parse(businessData) : this.businessData;
        this.businessExpansion = businessExpansion ? JSON.parse(businessExpansion) : this.businessExpansion;

        //turn of notifications in case any got stuck
        Object.values(this.businessExpansion).forEach(obj => obj.isOn = false);
        Object.values(this.managers).forEach(obj => obj.isOn = false);
    }
}

export interface ManagerConfig {
    isOn: boolean;
    isSold: boolean;
    price: number;
    count: number;
    info: string;
}

export interface ExpansionConfig extends ManagerConfig {
    multiplier: number;
}

export interface GameComponent{
    component: string;
    name?: string;
    x?: number|string;
    y?: number|string;
    iconName?: string;
    scale?:number;
    children?: GameComponent[]
}