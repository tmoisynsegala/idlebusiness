import * as PIXI from 'pixi.js';
import ResourceLoader from "./ResourceLoader";

export default class ProgressBar extends PIXI.Container{
    private readonly background:PIXI.Sprite;
    private readonly foreground:PIXI.Sprite;
    private readonly graphics = new PIXI.Graphics();
    private PROGRESS_WIDTH: number = 187;

    constructor() {
        super();

        this.pivot.set(0.5);
        this.background = new PIXI.Sprite(ResourceLoader.getAtlasTexture("progressbar_back"));
        this.foreground = new PIXI.Sprite(ResourceLoader.getAtlasTexture("progressbar_front"));

        this.background.anchor.set(0.5);
        this.foreground.anchor.set(0.5);

        this.addChild(this.background);
        this.addChild(this.foreground);

        this.foreground.mask = this.graphics;
    }

    setProgress(progress: number) {
        this.graphics.clear();
        this.graphics.beginFill(0xFF3300);
        let center = this.foreground.getGlobalPosition();
        //TODO Check how to properly remove this magic numbers
        this.graphics.drawRect(center.x-94, center.y-3, this.PROGRESS_WIDTH*progress, 8);
        this.graphics.endFill();
    }
}