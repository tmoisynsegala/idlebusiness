export const gameDescriptor = {
    assets: [
        {name: "bg", path: "background_8.png"},
        {name: "base", path: "base.json"},
    ],

    components: [
        {component:'PIXI.Sprite', image:'bg'},
        {
            component: 'PIXI.Container', children: [
                {name:'playerStats', component: 'PlayerStats', x: 0, y: 0, scale: 0.8}
            ]
        },
        {
            component: 'PIXI.Container', x: 600, y: 60, scale:0.9, children: [
                {name: 'farm', component: 'BusinessButton', x: 0, y: 0, iconName:'farm'},
                {name: 'store', component: 'BusinessButton', x: 0, y: 120, iconName:'store'},
                {name: 'sports', component: 'BusinessButton', x: 0, y: 240, iconName:'sports'},
                {name: 'car', component: 'BusinessButton', x: 0, y: 360, iconName:'car'},
                {name: 'diamond', component: 'BusinessButton', x: 0, y: 480, iconName:'diamond'},
                //second row
                {name: 'game', component: 'BusinessButton', x: 320, y: 0, iconName:'game'},
                {name: 'phone', component: 'BusinessButton', x: 320, y: 120, iconName:'phone'},
                {name: 'drug', component: 'BusinessButton', x: 320, y: 240, iconName:'drug'},
                {name: 'plane', component: 'BusinessButton', x: 320, y: 360, iconName:'plane'},
                {name: 'rocket', component: 'BusinessButton', x: 320, y: 480, iconName:'rocket'},
            ]
        },
        {
            component: 'PIXI.Container', x: 0, y: 180, children: [
                {name: 'notification1', component: 'NotificationButton', x: 168, y: 0},
                {name: 'notification2', component: 'NotificationButton', x: 168, y: 120},
                {name: 'notification3', component: 'NotificationButton', x: 168, y: 240},
            ]
        }
    ],
}