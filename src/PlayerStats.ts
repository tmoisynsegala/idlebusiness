import * as PIXI from 'pixi.js';
import ResourceLoader from "./base/ResourceLoader";

export default class PlayerStats extends PIXI.Container {
    private readonly bg: PIXI.Sprite;
    private readonly icon: PIXI.Sprite;
    private readonly text: PIXI.Text;

    constructor() {
        super();

        this.bg = new PIXI.Sprite(ResourceLoader.getAtlasTexture("bg_status"));
        this.icon = new PIXI.Sprite(ResourceLoader.getAtlasTexture("money"));
        this.text = new PIXI.Text("#0.123456789123456");

        this.text.style.fontFamily = "Balsamiq Sans";
        this.text.style.fontSize = 30;

        this.icon.scale.set(0.65);
        this.icon.x = 8;
        this.icon.y = 8;

        this.text.x = 150;
        this.text.y = 50;

        this.addChild(this.bg);
        this.addChild(this.icon);
        this.addChild(this.text);
    }

    setBalance(value: number) {
        this.text.text = "$ " + value;
    }
}