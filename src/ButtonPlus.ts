import * as PIXI from 'pixi.js';
import {TweenMax, Elastic} from 'gsap';
import ResourceLoader from "./base/ResourceLoader";

export default class ButtonPlus extends PIXI.Container {
    private readonly bg: PIXI.Sprite;
    private readonly icon: PIXI.Sprite;
    private readonly ORIGINAL_SCALE = 0.3;
    private readonly DISABLED = "button_add_disable";
    private readonly ENABLED = "button_add";

    constructor() {
        super();
        this.interactive = true;
        this.buttonMode = true;

        this.scale.set(this.ORIGINAL_SCALE);

        this.bg = new PIXI.Sprite(ResourceLoader.getAtlasTexture(this.DISABLED));
        this.icon = new PIXI.Sprite(ResourceLoader.getAtlasTexture("plus"));

        this.bg.anchor.set(0.5);

        this.icon.anchor.set(0.5);
        this.icon.scale.set(0.5);

        this.addChild(this.bg);
        this.addChild(this.icon);

        this.on("pointerdown", (e: PIXI.InteractionEvent) => this.onPointerDown(e));
        this.on("pointerup", (e: PIXI.InteractionEvent) => this.onPointerUp(e));
    }

    private onPointerDown(e: PIXI.InteractionEvent) {
        e.stopPropagation();
        const to = this.scale.x - this.scale.x * 0.1;
        TweenMax.to(this.scale, 0.05, {x: to, y: to});
    }

    private onPointerUp(e: PIXI.InteractionEvent) {
        e.stopPropagation();
        const to = this.ORIGINAL_SCALE;
        TweenMax.to(this.scale, 0.3, {x: to, y: to, ease:Elastic.easeOut});
    }

    public setStatus( enabled: boolean){
        this.interactive = enabled;
        this.buttonMode = enabled;
        this.bg.texture = ResourceLoader.getAtlasTexture(enabled ? this.ENABLED : this.DISABLED);
    }
}