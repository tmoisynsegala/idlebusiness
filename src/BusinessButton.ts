import * as PIXI from 'pixi.js';
import {TweenMax, Elastic} from 'gsap';
import ResourceLoader from "./base/ResourceLoader";
import ButtonPlus from "./ButtonPlus";
import ProgressBar from "./base/ProgressBar";

export default class BusinessButton extends PIXI.Container {
    private readonly bg: PIXI.Sprite;
    private readonly icon: PIXI.Sprite;
    private readonly buttonAdd: ButtonPlus;
    private readonly progressBar: ProgressBar;

    /**
     * Presents the value that will be paid by the end of time
     */
    private readonly textWon: PIXI.Text;

    /**
     * Show the cost of another purchase
     */
    private readonly textPrice: PIXI.Text;

    /**
     * Show the time until ready
     */
    private readonly textTime: PIXI.Text;

    /**
     * Show the number of purchases
     */
    private readonly textCount: PIXI.Text;

    public onBuy: Function = () => {console.warn('Callback not attributed, called')};
    public onWork: Function = () => {console.warn('Callback not attributed, called')};

    constructor(iconName: string) {
        super();

        this.pivot.set(0.5);

        this.progressBar = new ProgressBar();
        this.bg = new PIXI.Sprite(ResourceLoader.getAtlasTexture("button"));
        this.icon = new PIXI.Sprite(ResourceLoader.getAtlasTexture(iconName));
        this.buttonAdd = new ButtonPlus();
        this.textWon = new PIXI.Text("$4444");
        this.textPrice = new PIXI.Text("$11");
        this.textTime = new PIXI.Text("00:00:00");
        this.textCount = new PIXI.Text("#5444");

        this.textWon.style.fontFamily = "Balsamiq Sans";
        this.textTime.style.fontFamily = "Balsamiq Sans";
        this.textCount.style.fontFamily = "Balsamiq Sans";
        this.textPrice.style.fontFamily = "Balsamiq Sans";

        //TODO Move all configs the the game-descriptor
        this.textWon.anchor.set(1, 0);
        this.textPrice.anchor.set(0, 1);
        this.textTime.anchor.set(1);

        this.textWon.x = 140;
        this.textWon.y = -42;
        this.textPrice.x = -90;
        this.textPrice.y = 40;
        this.textTime.x = 140;
        this.textTime.y = 40;
        this.textCount.x = -90;
        this.textCount.y = -42;

        this.progressBar.scale.set(0.3);
        this.progressBar.x = 24;

        this.bg.anchor.set(0.5);

        this.icon.x = -120;
        this.icon.y = -23;
        this.icon.anchor.set(0.5);
        this.icon.scale.set(0.3);

        this.buttonAdd.x = -120;
        this.buttonAdd.y = 20;
        this.buttonAdd.setStatus(false);
        this.buttonAdd.on("pointerup", (e:PIXI.InteractionEvent) => {
            this.onBuy();
        })

        this.addChild(this.bg);
        this.addChild(this.icon);
        this.addChild(this.buttonAdd);
        this.addChild(this.progressBar);
        this.addChild(this.textWon);
        this.addChild(this.textPrice);
        this.addChild(this.textTime);
        this.addChild(this.textCount);

        this.on("pointerdown", (e: PIXI.InteractionEvent) => this.onPointerDown(e));
        this.on("pointerup", (e: PIXI.InteractionEvent) => this.onPointerUp(e));
    }

    private onPointerDown(e: PIXI.InteractionEvent) {
        TweenMax.to(this.scale, 0.05, {x: 0.9, y: 0.9});
    }

    private onPointerUp(e: PIXI.InteractionEvent) {
        TweenMax.to(this.scale, 0.3, {x: 1, y: 1, ease: Elastic.easeOut});
        this.onWork();
    }

    public config(config: BusinessConfig, progress:number) {
        const {price, count, time, won} = config;
        this.textPrice.text = "$" + price;
        this.textCount.text = "#" + count;
        this.textTime.text = `${Math.floor(time)} s`;
        this.textWon.text = "$" + won * count;

        this.progressBar.setProgress(progress);
    }

    setEnablePurchase(enabled: boolean) {
        this.buttonAdd.setStatus(enabled);
    }

    setEnable(enable: boolean) {
        this.interactive = enable;
        this.buttonMode = enable;
    }
}


export interface BusinessConfig {
    /**
     * Price to pay for every single unit
     */
    price: number;
    /**
     * Number of owned business
     */
    count: number;
    /**
     * Current running time
     */
    time: number;
    /**
     * Setup time, this is the time to reset to
     */
    workTime: number;
    /**
     * This value will be paid when work time ends
     */
    won: number;

    isRunning:boolean;

    /**
     * The time whe the work has started
     */
    startTime: number;
}