import * as PIXI from 'pixi.js';
import {TweenMax, Elastic} from 'gsap';
import ResourceLoader from "./base/ResourceLoader";
import {ManagerConfig} from "./base/Game";

export default class NotificationButton extends PIXI.Container {
    private readonly bg: PIXI.Sprite;
    private readonly icon: PIXI.Sprite;
    private readonly textInfo: PIXI.Text;
    private readonly textPrice: PIXI.Text;
    private _isEmpty: boolean = true;
    public onSold: Function = () => {
        console.warn('Callback not attributed, called')
    };

    constructor() {
        super();

        this.scale.set(0);
        this.pivot.set(0.5);
        this.interactive = true;
        this.buttonMode = true;

        this.bg = new PIXI.Sprite(ResourceLoader.getAtlasTexture("button"));
        this.icon = new PIXI.Sprite(ResourceLoader.getAtlasTexture("manager"));
        this.textInfo = new PIXI.Text("New Manager");
        this.textPrice = new PIXI.Text("$1000");

        this.textInfo.style.fontFamily = "Balsamiq Sans";
        this.textPrice.style.fontFamily = "Balsamiq Sans";
        this.textInfo.style.fontSize = 20;

        this.bg.anchor.set(0.5);

        this.icon.x = -150;
        this.icon.y = -40;
        this.icon.pivot.set(0.5);
        this.icon.scale.set(0.4);

        this.textInfo.x = -60;
        this.textInfo.y = -30;

        this.textPrice.x = -60;
        this.textPrice.y = 0;

        this.addChild(this.bg);
        this.addChild(this.icon);
        this.addChild(this.textInfo);
        this.addChild(this.textPrice);

        this.on("pointerdown", (e: PointerEvent) => this.onPointerDown(e));
        this.on("pointerup", (e: PointerEvent) => this.onPointerUp(e));
    }

    private onPointerDown(e: PointerEvent) {
        TweenMax.to(this.scale, 0.05, {x: 0.9, y: 0.9});
    }

    private onPointerUp(e: PointerEvent) {
        TweenMax.to(this.scale, 0.3,
            {
                x: 1, y: 1, ease: Elastic.easeOut, onComplete: () => {
                    this.hide();
                    this._isEmpty = true;
                }
            });
        this.onSold();
    }

    isEmpty(): boolean {
        return this._isEmpty;
    }

    config(manager: ManagerConfig) {
        this.textPrice.text = "$" + manager.price;
        this.textInfo.text = manager.info;
        this._isEmpty = false;
    }

    show() {
        TweenMax.to(this.scale, 0.05, {x: 1, y: 1});
    }

    hide() {
        TweenMax.to(this.scale, 0.15, {x: 0, y: 0});
    }

    disable() {
        TweenMax.to(this.scale, 0.15, {x: 0, y: 0, delay: 1});
        this._isEmpty = true;
    }
}